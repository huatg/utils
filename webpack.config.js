const path = require('path')

module.exports = {
  mode: 'development',
  // 入口
  entry: './src/index.js',
  // 出口
  output: {
    // 打包的路径
    path: path.resolve(__dirname, 'lib'),
    // 打包文件
    filename: 'utils-web.js',
    // 设置对外暴露对象的全局名称
    library: 'webUtils',
    // 打包生成通过esm，commonjs，requirejs的语法引入
    libraryTarget: 'umd',
  },
}
