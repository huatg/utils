let path = process.env.NODE_ENV === 'development' ? '/' : '/utils/'
module.exports = {
  title: 'utils-web',
  description: 'js常用工具封装',
  base: path,
  mode: 'hash',
  // // 设置 favicon.ico，注意图片放在 public 文件夹下
  head: [['link', { rel: 'icon', href: 'favicon.ico' }]],
  configureWebpack: {
    resolve: {
      alias: {
        '@alias': './assets/',
        '@c': './components/',
      },
    },
  },
  dest: 'utils',
  plugins: [
    'demo-container',
    '@vuepress-reco/extract-code',
    [
      'vuepress-plugin-auto-sidebar',
      {
        titleMap: {
          'exampleSubMenu1-a': '🎉 Hello Vuepress 🎉',
          titleMode: 'default',
        },
      },
    ],
  ],
  themeConfig: {
    logo: '/logo.png',
    nav: [
      { text: '首页', link: '/' },
      { text: '说明', link: '/webUtils/' },
      { text: '仓库地址', link: 'https://gitee.com/huatg/utils.git' },
      { text: '意见反馈', link: 'https://gitee.com/huatg/utils/issues/I4DO12' },
    ],
    sidebarDepth: 3,
  },
}
