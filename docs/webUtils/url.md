---
title: url
sibarDepth: 1
autoGroup-1: 方法说明
---

### 从访问地址获取 key 对应的 value

```js
webUtils.getQueryString('key')
```
