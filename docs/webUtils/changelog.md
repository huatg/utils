---
title: 更新日志
sibarDepth: 1
---

### 2022.02.57

-   关于 cookies 的设置与获取
-   关于获取 url 的方法

### 2022.05.53

-   添加判断环境的方法

### 2021.10.13

-   添加 getArrItem

### 意见收集

-   在这里你可以发表你的意见，作者看到会第一时间回复

<a class='el-icon-tickets' href="https://gitee.com/huatg/utils/issues/I4DO12">收集意见</a>

### qq 交流群

-   117667389
