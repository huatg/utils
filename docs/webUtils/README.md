### 安装方式

#### npm

```js
    npm install utils-web

    const webUtils = require('utils-web')
    webUtils.demo()
```

#### cdn

```js
<script src="https://unpkg.com/utils-web/lib/utils-web.js"></script>
<script type="text/javascript">
    webUtils.demo()
</script>

```

#### 在这里你可以留下你的意见啊

<a class='el-icon-tickets' href='https://gitee.com/huatg/utils/issues/I4DO12'> 意见反馈 </a>
