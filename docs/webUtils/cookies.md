---
title: 关于cookies 设置

sibarDepth: 1
autoGroup-1: 方法说明
---

## 设置 cookies

```js
webUtils.setCookie('key', 'value')
```

## 获取 cookies

```js
webUtils.getCookie('key')
```
