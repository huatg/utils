---
title: getArrItem
sibarDepth: 1
autoGroup-1: 方法说明
---

### 获取数组指定的 id 的项，其他属性值

> getArrItem 方法

- 如下 demo 根据 id 2 查找 对应的 dictKey 对应的属性值

```js
// 在一个数组中，根据id找对应的数据

const arr = [
  {
    id: 1,
    dictKey: '单选',
    dictValue: '1',
  },
  {
    id: 2,
    dictKey: '输入',
    dictValue: '3',
  },
  {
    id: 3,
    dictKey: '多选',
    dictValue: '2',
  },
]

const _arrKey = webUtils.getArrItem(arr, 2, 'dictKey')

console.log('_arrKey', _arrKey)

// _arrKey 输入
```
