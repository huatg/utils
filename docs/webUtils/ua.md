---
title: navigator
sibarDepth: 1
autoGroup-1: 方法说明
---

## 判断是否是微信环境

```js
webUtils.ua.isWeixin() // true 是，false 不是
```

## 判断 isAndroid

```js
webUtils.ua.isAndroid() // true 是，false 不是
```

## 判断 isIos

```js
webUtils.ua.isIos() // true 是，false 不是
```
