(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["webUtils"] = factory();
	else
		root["webUtils"] = factory();
})(window, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/function/cookies.js":
/*!*********************************!*\
  !*** ./src/function/cookies.js ***!
  \*********************************/
/*! exports provided: setCookie, getCookie */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"setCookie\", function() { return setCookie; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getCookie\", function() { return getCookie; });\nconst setCookie = (cname, cvalue, exdays) => {\r\n    var d = new Date()\r\n    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1e3)\r\n    var expires = 'expires=' + d.toGMTString()\r\n    document.cookie = cname + '=' + cvalue + '; ' + expires\r\n}\r\n\r\nconst getCookie = name => {\r\n    var arr,\r\n        reg = new RegExp('(^| )' + name + '=([^;]*)(;|$)')\r\n    if ((arr = document.cookie.match(reg))) return unescape(arr[2])\r\n    else return ''\r\n}\r\n\n\n//# sourceURL=webpack://webUtils/./src/function/cookies.js?");

/***/ }),

/***/ "./src/function/demo.js":
/*!******************************!*\
  !*** ./src/function/demo.js ***!
  \******************************/
/*! exports provided: demo */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"demo\", function() { return demo; });\nconst demo = () => {\r\n    console.log('测试这个方法~~哈这是一个小测试！')\r\n}\r\n\n\n//# sourceURL=webpack://webUtils/./src/function/demo.js?");

/***/ }),

/***/ "./src/function/getArrId.js":
/*!**********************************!*\
  !*** ./src/function/getArrId.js ***!
  \**********************************/
/*! exports provided: getArrItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getArrItem\", function() { return getArrItem; });\n// 在一个数组中，根据id找对应的数据\r\n\r\nconst getArrItem = (arr, _id, value) => {\r\n    return arr.find(item => {\r\n        return item.id == _id\r\n    })[value]\r\n}\r\n\n\n//# sourceURL=webpack://webUtils/./src/function/getArrId.js?");

/***/ }),

/***/ "./src/function/navigator.js":
/*!***********************************!*\
  !*** ./src/function/navigator.js ***!
  \***********************************/
/*! exports provided: isMobile, isWeixin, isAndroid, isIos */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"isMobile\", function() { return isMobile; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"isWeixin\", function() { return isWeixin; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"isAndroid\", function() { return isAndroid; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"isIos\", function() { return isIos; });\n/**\n * 判断是否移动设备访问\n * @returns {boolean} true 移动设备  false pc设备\n */\nconst isMobile = () => {\n    return /iphone|ipod|android.*mobile|windows.*phone|blackberry.*mobile/i.test(\n        window.navigator.userAgent.toLowerCase()\n    )\n}\n\n/**\n * 判断是否微信内置浏览器环境\n * @returns {boolean} true 微信环境  false 浏览器\n */\nconst isWeixin = () => {\n    const ua = navigator.userAgent.toLowerCase()\n    const uaMatch = ua.match(/MicroMessenger/i)\n    if (uaMatch == 'micromessenger') {\n        return true\n    }\n    return false\n}\n\n/**\n * 判断运行环境是安卓还是IOS\n * @returns  {boolean} true => 安卓 false => IOS\n */\nconst isAndroid = () => {\n    let u = navigator.userAgent\n    let isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1 //android终端\n    // let isiOS = !!u.match(/\\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端\n    return isAndroid ? true : false\n}\n\nconst isIos = () => {\n    console.log('454')\n    let u = navigator.userAgent\n    let isiOS = !!u.match(/\\(i[^;]+;( U;)? CPU.+Mac OS X/) //ios终端\n    return isiOS ? true : false\n}\n\nconsole.log('ua', window.navigator.userAgent)\n\n\n//# sourceURL=webpack://webUtils/./src/function/navigator.js?");

/***/ }),

/***/ "./src/function/url.js":
/*!*****************************!*\
  !*** ./src/function/url.js ***!
  \*****************************/
/*! exports provided: getQueryString */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getQueryString\", function() { return getQueryString; });\n/**\r\n * @desc 获取url参数\r\n * @param {String} name  想要获取的参数名字\r\n */\r\nconst getQueryString = name => {\r\n    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)')\r\n    var r = window.location.search.substr(1).match(reg)\r\n    if (r != null) return unescape(r[2])\r\n    return null\r\n}\r\n\r\n// 调用方式\r\n// var objectId = getQueryString('objectId')\r\n\n\n//# sourceURL=webpack://webUtils/./src/function/url.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! exports provided: demo, getArrItem, ua, getQueryString, setCookie, getCookie */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _function_demo_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./function/demo.js */ \"./src/function/demo.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"demo\", function() { return _function_demo_js__WEBPACK_IMPORTED_MODULE_0__[\"demo\"]; });\n\n/* harmony import */ var _function_getArrId__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./function/getArrId */ \"./src/function/getArrId.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"getArrItem\", function() { return _function_getArrId__WEBPACK_IMPORTED_MODULE_1__[\"getArrItem\"]; });\n\n/* harmony import */ var _function_url__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./function/url */ \"./src/function/url.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"getQueryString\", function() { return _function_url__WEBPACK_IMPORTED_MODULE_2__[\"getQueryString\"]; });\n\n/* harmony import */ var _function_navigator__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./function/navigator */ \"./src/function/navigator.js\");\n/* harmony reexport (module object) */ __webpack_require__.d(__webpack_exports__, \"ua\", function() { return _function_navigator__WEBPACK_IMPORTED_MODULE_3__; });\n/* harmony import */ var _function_cookies__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./function/cookies */ \"./src/function/cookies.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"setCookie\", function() { return _function_cookies__WEBPACK_IMPORTED_MODULE_4__[\"setCookie\"]; });\n\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"getCookie\", function() { return _function_cookies__WEBPACK_IMPORTED_MODULE_4__[\"getCookie\"]; });\n\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\n\n//# sourceURL=webpack://webUtils/./src/index.js?");

/***/ })

/******/ });
});