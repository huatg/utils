/**
 * @desc 获取url参数
 * @param {String} name  想要获取的参数名字
 */
export const getQueryString = name => {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)')
    var r = window.location.search.substr(1).match(reg)
    if (r != null) return unescape(r[2])
    return null
}

// 调用方式
// var objectId = getQueryString('objectId')
