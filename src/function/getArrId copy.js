// 在一个数组中，根据id找对应的数据

const arr = [
    {
        id: 1,
        dictId: 3,
        dictType: 'ATTR_TYPE',
        dictKey: '单选',
        dictValue: '1',
        isDefault: 0
    },
    {
        id: 2,
        dictId: 3,
        dictType: 'ATTR_TYPE',
        dictKey: '输入',
        dictValue: '3',
        isDefault: 0
    },
    {
        id: 3,
        dictId: 3,
        dictType: 'ATTR_TYPE',
        dictKey: '多选',
        dictValue: '2',
        isDefault: 0
    }
]

const _arrKey = getArrItem(arr, 2, 'dictKey')

console.log('_arrId', _arrKey)

// _arrId 输入
