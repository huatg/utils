import { demo } from './function/demo.js'
import { getArrItem } from './function/getArrId'
import { getQueryString } from './function/url'

import * as ua from './function/navigator'
import { setCookie, getCookie } from './function/cookies'
export { demo, getArrItem, ua, getQueryString, setCookie, getCookie }
