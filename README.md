### js 工具库

> 通过工作集累的 js 工具。

### 安装方式

#### npm 方法

```js
    npm install utils-web

    const webUtils = require('utils-web')
    webUtils.demo()
```

#### cdn

```js
<script src="https://unpkg.com/utils-web/lib/utils-web.js"></script>
<script type="text/javascript">
    webUtils.demo()
</script>

```

### 使用说明文档

-   http://huatg.gitee.io/utils/

### 交流群 qq 群

-   117667389
