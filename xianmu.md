## npm 发布流程

1. npm 登录

```js
npm login

```

2. 安照提示输入用户名与密码， 用户名与密码在上面。登录成功后
3. 发布 执行命令就行

```js
 npm publish
```

### 发布的时候，剔除不需要的目录

-   配置 `.npmignore` 文件

```js
/src/
/docs/
/md.md
/README.md

```

### 配置代码格式化

-   配置文件 `prettier.config.js`

```js
module.exports = {
    htmlWhitespaceSensitivity: 'ignore',
    printWidth: 120, // 每行代码长度（默认80）
    semi: false, // 声明结尾使用分号(默认true)
    singleQuote: true,
    tabWidth: 4,
    trailingComma: 'none'
}
```

-   运行 `npm run build`
-   打包 `npm run docs:build`

## 正在整理的

-   https://blog.csdn.net/guanguan0_0/article/details/83993324
